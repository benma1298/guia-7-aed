#include <iostream>
#include <fstream>
#include <cstring>
#include "primcode.h"
#define INF  5000000

using namespace std;

//Constructor de la clase
primcode::primcode(){

}

//Funcion que aplica el algoritmo PRIM a la matriz
//creada por los datos del el usuario.
void primcode::algoritmPrim(int P, int **G, string *Nodo){
    int vertice;
    int *costo;
    int *firstNodo;
    int *lastNodo;

    costo = new int[P];
    firstNodo = new int[P];
    lastNodo = new int[P];

    int det[P];
    memset(det, false, sizeof(det));
    vertice = 0;
    det[0] = true;

    //numero de filas.
    int fil;

    //numero de columnas.
    int col;

    cout << "(x, y)"
    << " : "
    << "Costo "
    << ": L = {}";
    cout << endl;


    while(vertice < P-1){
        int minimo = INF;
        fil = 0;
        col = 0;

        for(int i = 0; i < P; i++){
            if(det[i]){
                for(int j = 0; j < P; j++){
                    if(!det[j] && G[i][j]){
                        if(minimo >= G[i][j]){
                            minimo = G[i][j];
                            fil = i;
                            col = j;
                        }
                    }
                }
            }
        }

        if(fil != col){
            costo[vertice] = G[fil][col];
            firstNodo[vertice] = fil;
            lastNodo[vertice] = col;

            cout << "(" << Nodo[fil] << ", " << Nodo[col] << "): " << G[fil][col] << ":";
        }

        det[col] = true;
        vertice++;

        for(int i = 0; i < vertice; i++){
            cout << "(" << Nodo[firstNodo[i]] << ", " << Nodo[lastNodo[i]] << ")";
        }
        cout << endl;
    }
    crearGrafoPrim(P, costo, Nodo, firstNodo, lastNodo);
}

//Funcion que crea e imprime el grafo correspondiente a los datos
//determinados por la aplicacion del algoritmo PRIM.
void primcode::crearGrafoPrim(int P, int *G, string *vector, int *X, int *Y){
    ofstream archivo("grafo.txt", ios::out);
    archivo << "graph G {" << endl;
    archivo << "graph [rankdir = LR]" << endl;
    archivo << "node [style=filled fillcolor=green]" << endl << endl;

    for(int i = 0; i < P-1; i++){
        archivo << vector[X[i]] << " -- " << vector[Y[i]] << " ";
        archivo << "[label=" + to_string(G[i]) << "];" << endl;
    }
    archivo <<  "} ";
    archivo.close();
    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}

//Funcion que genera e imprime el grafo original usando los datos
//determinados por el usuario en la terminal.
void primcode::crearGrafo(int P, int **G, string *vector){
    ofstream archivo("grafoInicial.txt", ios::out);
    archivo << "graph G {" << endl;
    archivo << "graph [rankdir = LR]" << endl;
    archivo << "node [style=filled fillcolor=green]" << endl << endl;

    for(int i = 0; i < P; i++){
        for(int j = 0; j < P; j++){
            if(j > i && i != j && G[i][j]){
                archivo << vector[i] << " -- " << vector[j] << " ";
                archivo << "[label=" + to_string(G[i][j]) << "];" << endl;
            }
        }
    }
    archivo << "}";
    archivo.close();
    system("dot -Tpng -ografoInicial.png grafoInicial.txt");
    system("eog grafoInicial.png &");
}
