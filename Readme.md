Guia 7 Algoritmos y Estructura de datos
Unidad II
Algoritmo de PRIM

Autor Benjamin Martin.

Este programa nos permite generar, a partir de datos ingresados por el usuario, un grafo dada una matriz de distancias, determinadas por el usuario en la terminal.
A esta matriz se le aplica el algoritmo PRIM, el cual determina, a partir de la matriz creada por el usuario, el grafo con el minimo peso posible.
Finalizando el programa, se generan dos grafos, "grafo.png" el cual es el grafo resultante de la aplicacion del algoritmo de PRIM; y "grafoInicial.png" el cual corresponde al grafo original en donde no ha sido aplicado el algoritmo PRIM.

Los pasos para la ejecucion del programa son:
    Paso 1: Abrir terminal e ingresar el comando "make".
    Paso 2: Luego, digitar el comando ./main seguido del numero de Nodos que se desea generar. ejemplo: "./main 'numero entero mayor a 2'"
    Paso 3: Se le pedira al usuario determinar los nombres de cada Nodo.
    Paso 4: Se le pedira al usuario determinar las distancias de un Nodo en especifico con los demas restantes.
    Paso 5: El programa, usando los datos entregados, aplica el algoritmo PRIM, dando como resultado los Grafos "grafo.png" y "grafoInicial.png".