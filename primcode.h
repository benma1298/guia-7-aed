#ifndef PRIMCODE_H
#define PRIMCODE_H

#include <iostream>
#include <fstream>

using namespace std;

class primcode{
    private:

    public:
    //Constructor de la clase.
    primcode();
    
    //Funcion que inicia al algoritmo PRIM.
    void algoritmPrim(int P, int **Matriz, string *primalg);

    //Metodo que crea el grafo del algoritmo PRIM y luego lo imprime.
    void crearGrafoPrim(int P, int *Matriz, string *vector, int *X, int *Y);

    //Metodo que crea el grafo original determinado por el usuario y luego lo imprime.
    void crearGrafo(int P, int **Matriz, string *vector);
};

#endif