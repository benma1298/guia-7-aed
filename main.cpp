#include <iostream>
#include <fstream>
#include <cstring>
#include "primcode.h"

using namespace std;


// Funcion que crea la matriz de dimensiones solicitadas al usuario.
void crearMatriz(int num, string *Nodo, int **Matriz){
    int dis;
    //El primer ciclo for determina el nodo en uso.
    for(int i = 0; i < num; i++){
        cout << "Determine las distancias del nodo " << Nodo[i] << ": ";
        cout << "\n" << endl;
        //El segundo ciclo for recibe las distancias a los nodos respectivos.
        for(int j = 0; j < num; j++){
            if(j>i && i!=j){
                cout << "Distancia al nodo " << Nodo[j] << ": ";
                cin >> dis;
                Matriz[i][j] = dis;
            }
            //Se genera la diagonal de 0 de la matriz.
            else{
                Matriz[i][j] = 0;
            }
        }
    }
}

// Funcion que determina el nombre del Nodo, dado por el usuario.
void nombrarNodo(int num, string *Nodo){
    string name;
    //Ciclo for que dependiendo del num determinado por el usuario
    //determina los nombres de cada nodo.
    for(int i = 0; i < num; i++){
        cout << "Determine el nombre del Nodo " << i+1 << ": ";
        cin >> name;
        Nodo[i] = name;
        cout << "\n" << endl;
    }
}

// Funcion main.
int main(int argc, char **argv){
    //Variable que recibe el numero de nodos determinados por el usuario. 
    int num;
    num = atoi(argv[1]);
    string Nodo[num];

    //Se crea la Matriz.
    int **Matriz;
    Matriz = new int*[num];
    for(int i = 0; i < num; i++){
        Matriz[i] = new int[num];
    }
    primcode primcode;
    
    //Se verifica si la variable num es mayor a 2.
    if(num <= 2){
        cout << "ERROR, el valor de num debe ser mayor a 2." << endl;
        return -1;
    }
    //Se llama a la funcion que nombra a cada nodo.
    nombrarNodo(num, Nodo);
    //Se llama a la funcion que crea a la Matriz.
    crearMatriz(num, Nodo, Matriz);

    //Se llama a la funcion, ubicada en la clase primcode, para usar el algortimo PRIM.
    primcode.algoritmPrim(num, Matriz, Nodo);
    //Se llama a la funcion, ubicada en la clase primcode, que crea e imprime los grafos.
    primcode.crearGrafo(num, Matriz, Nodo);

    //Se elimina la Matriz.
    delete(Matriz);
    return 0;
}
